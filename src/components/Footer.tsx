import React, { HTMLAttributes } from "react";
import { IContacts, IDownload } from "types";
import Logo from "components/common/Logo";
import Download from "components/common/Download";
import Contacts from "components/common/Contacts";
import Button from "components/common/Button";

import styles from "assets/styles/components/Footer.module.scss";

type Props = IContacts & HTMLAttributes<HTMLDivElement>;
interface IProps extends Props {
  downloads: IDownload[];
  openDialog?: Function;
}

const Footer: React.FC<IProps> = ({
  phone,
  address,
  downloads,
  openDialog,
}) => (
  <div className={styles.wrap}>
    <div className={styles.logo}>
      <Logo />
      <div>&copy; 2017 - {new Date().getFullYear()}</div>
    </div>
    <div className={styles.downloads}>
      {downloads.map(({ title, src }, key) => (
        <Download title={title} src={src} className={styles.item} key={key} />
      ))}
    </div>
    <Contacts
      className={styles.contacts}
      phone={phone}
      address={address}
      invert
    />
    <Button className={styles.button} action={openDialog}>
      Хочу попробовать
    </Button>
  </div>
);

export default Footer;
