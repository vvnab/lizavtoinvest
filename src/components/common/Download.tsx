import React, { HTMLAttributes } from "react";
import Image from "next/image";
import downloadIcon from "../../assets/icons/download.svg";
import { IDownload } from "types";

import styles from "assets/styles/components/common/Download.module.scss";

type Props = IDownload & HTMLAttributes<HTMLDivElement>;
interface IProps extends Props {
  invert?: boolean;
}

const Download: React.FC<IProps> = ({ className, title, src, invert }) => (
  <div className={[styles.wrap, className].join(" ")}>
    <div className={[styles.icon, invert ? styles.invert : ""].join(" ")}>
      <Image  alt="" src={downloadIcon} />
    </div>
    <a href={src} className={styles.href} rel="noopener nofollow noreferrer noindex">
      {title}
    </a>
  </div>
);

export default Download;
