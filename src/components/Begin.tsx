import React from "react";
import Button from "components/common/Button";
import styles from "assets/styles/components/Begin.module.scss";

interface IProps {
  openDialog?: Function;
}

const Begin: React.FC<IProps> = ({ openDialog }) => (
  <div className={styles.wrap}>
    <div className={styles.inner}>
      <div className={styles.left}>
        <div className={styles.title}>Как начать с нами работу?</div>
        <Button type="dark" className={styles.button} action={openDialog}>
          Хочу попробовать
        </Button>
      </div>

      <div className={styles.over}>
        <div className={styles.line}></div>
        <div className={styles.item}>
          <div className={styles.number}>1</div>
          <div className={styles.text}>
            <Button action={openDialog} type="simple" className={styles.link}>
              Заполни Заявку
            </Button>{" "}
            на сайте или позвони нам
          </div>
        </div>
        <div className={styles.item}>
          <div className={styles.number}>2</div>
          <div className={styles.text}>
            Наш специалист свяжется с Вами в течение 48 часов и предложит
            варианты сотрудничества
          </div>
        </div>

        <Button type="invert" className={styles.button} action={openDialog}>
          Хочу попробовать
        </Button>
      </div>
    </div>
    <div className={styles.over2}></div>
  </div>
);

export default Begin;
