const postToJira = async (values: any): Promise<any> => {
  const result = await fetch("/api/jira", {
    method: "POST",
    body: JSON.stringify(values),
  });
  return result;
};

export { postToJira };
