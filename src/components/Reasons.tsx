import React, { HTMLAttributes } from "react";
import Image from "next/image";
import ReactMarkdown from "react-markdown";
import { IReasons, IReason } from "types";
import shieldIcon from "assets/icons/shield.svg";
import starIcon from "assets/icons/star.svg";
import painIcon from "assets/icons/pain.svg";
import warningIcon from "assets/icons/warning.svg";

import styles from "assets/styles/components/Reasons.module.scss";

type Props = HTMLAttributes<HTMLDivElement> & IReasons;

const Reasons: React.FC<Props> = ({
  risks,
  pluses,
  competitorsPluses,
  competitorsRisks,
}) => (
  <div className={styles.wrap}>
    <div className={styles.row}>
      <Reason
        title="Риски и пути решения"
        reasons={risks}
        icon={shieldIcon}
        subtitle={
          <div className={[styles.subtitle, styles.green].join(" ")}>
            Решение:
          </div>
        }
      />
      <Reason title="Плюсы работы с нами" reasons={pluses} icon={starIcon}>
        <div className={styles.addon}>Мы стремимся свести Ваши риски к 0!</div>
      </Reason>
    </div>
    <div className={styles.row}>
      <Reason
        title="Риски работы с нашими конкурентами"
        reasons={competitorsRisks}
        icon={painIcon}
      />
      <Reason
        title="Плюсы работы с нашими конкурентами"
        reasons={competitorsPluses}
        icon={warningIcon}
        subtitle={
          <div className={[styles.subtitle, styles.red].join(" ")}>
            Но кто отвественен за:
          </div>
        }
      />
    </div>
  </div>
);

interface IReasonProps extends HTMLAttributes<HTMLDivElement> {
  icon: any;
  title: string;
  reasons: IReason[];
  subtitle?: React.ReactChild;
}

const Reason: React.FC<IReasonProps> = ({
  icon,
  children,
  title,
  reasons,
  subtitle,
}) => (
  <div className={styles.reasons}>
    <div className={styles.icon}>
      <Image alt="" src={icon} />
    </div>
    <div className={styles.title}>{title}</div>
    {reasons.map(({ title, description }, key) => (
      <ReasonItem
        key={key}
        icon={icon}
        title={title}
        description={description}
        subtitle={subtitle}
      />
    ))}
    {children}
  </div>
);

interface IReasonItemProps extends HTMLAttributes<HTMLDivElement> {
  icon: any;
  title: string;
  subtitle?: React.ReactChild;
  description?: string;
}

const ReasonItem: React.FC<IReasonItemProps> = ({
  title,
  subtitle,
  description,
  icon,
}) => (
  <div className={styles.reasonItem}>
    <div className={styles.icon}>
      <Image alt="" src={icon} />
    </div>
    <div className={styles.text}>
      <div className={styles.title}>{title}</div>
      {subtitle}
      {description && (
        <ReactMarkdown className={styles.description}>
          {description}
        </ReactMarkdown>
      )}
    </div>
  </div>
);

export default Reasons;
