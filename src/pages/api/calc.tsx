import type { NextApiRequest, NextApiResponse } from "next";
import { renderToStream } from "@react-pdf/renderer";
import Calc from "components/CalcPDF";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  let { cars, town, term, schedule } = req.query;
  cars = cars instanceof Array ? cars : [cars];
  const buffer = await renderToStream(
    <Calc {...{ term, town, schedule, cars }} />
  );
  // res.setHeader("Content-disposition", 'attachment; filename="article.pdf');
  res.setHeader("Content-disposition", "inline");
  res.setHeader("Content-Type", "application/pdf");
  res.send(buffer);
}
