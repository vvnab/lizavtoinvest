export default function getWordEnding(
  value: string | number,
  ending: [string, string, string],
) {
  value = value.toString()
  let num = value.slice(value.length - 2)

  if (num === '11' || num === '12' || num === '13' || num === '14') {
    return ending[2]
  }

  num = value.slice(-1)

  if (num === '1') {
    return ending[0]
  } else if (num === '2' || num === '3' || num === '4') {
    return ending[1]
  } else {
    return ending[2]
  }
}
