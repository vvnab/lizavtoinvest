import React, { useState } from "react";
import type { NextPage, GetStaticProps } from "next";
import Head from "next/head";
import Header from "components/Header";
import Hero from "components/Hero";
import HowItWorks from "components/HowItWorks";
import Application from "components/Application";
import Reasons from "components/Reasons";
import Begin from "components/Begin";
import Faqs from "components/Faqs";
import Footer from "components/Footer";
import {
  IDownload,
  IHowItWorks,
  ICar,
  IReasons,
  IContacts,
  IFaqs,
  IHero,
  IMeta,
  IApplication,
} from "types";
import { loadManyContentFiles } from "utils/content";

import styles from "assets/styles/pages/index.module.scss";

interface Props {
  downloads: IDownload[];
  howitworks: IHowItWorks;
  faqs: IFaqs;
  cars: ICar[];
  reasons: IReasons;
  contacts: IContacts;
  hero: IHero;
  meta: IMeta;
  application: IApplication;
  messages: any;
}

const Home: NextPage<Props> = ({
  downloads,
  howitworks,
  faqs,
  cars,
  reasons,
  contacts,
  hero,
  meta,
  application,
  messages,
}) => {
  const [dialogOpened, setDialogOpened] = useState<boolean>(false);

  return (
    <div className={styles.layout}>
      <Head>
        <title>{meta.title}</title>
        <meta name="description" content={meta.description} />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <Header downloads={downloads} contacts={contacts} />
      <Hero {...hero} openDialog={() => setDialogOpened(true)} />
      <HowItWorks
        {...howitworks}
        downloads={downloads}
        openDialog={() => setDialogOpened(true)}
      />
      <Application
        cars={cars}
        application={application}
        dialogOpened={dialogOpened}
        setDialogOpened={setDialogOpened}
        messages={{ ...messages?.submit }}
      />
      <Reasons {...reasons} />
      <Begin openDialog={() => setDialogOpened(true)} />
      <Faqs faqs={faqs} />
      <Footer
        downloads={downloads}
        {...contacts}
        openDialog={() => setDialogOpened(true)}
      />
    </div>
  );
};

export default Home;

export async function getStaticProps(context: GetStaticProps) {
  const [
    cars,
    reasons,
    faqs,
    contacts,
    downloads,
    howitworks,
    hero,
    meta,
    application,
    messages,
  ] = await loadManyContentFiles([
    "../content/cars.yaml",
    "../content/reasons.yaml",
    "../content/faqs.yaml",
    "../content/contacts.yaml",
    "../content/downloads.yaml",
    "../content/howItWorks.yaml",
    "../content/hero.yaml",
    "../content/meta.yaml",
    "../content/application.yaml",
    "../content/messages.yaml",
  ]);

  return {
    props: {
      cars,
      reasons,
      faqs,
      contacts,
      downloads,
      howitworks,
      hero,
      meta,
      application,
      messages,
    },
  };
}
