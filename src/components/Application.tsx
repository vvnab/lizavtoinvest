import React, { HTMLAttributes, useState } from "react";
import Image from "next/image";
import Slider from "react-slick";
import useWindowSize from "hooks/useWindowSize";
import checkIcon from "assets/icons/check.svg";
import checkInverseIcon from "assets/icons/check-inverse.svg";
import Calculator from "components/Calculator";
import Price from "components/common/Price";
import { ICar, ISelectedCars, IApplication } from "types";

import styles from "assets/styles/components/Application.module.scss";

const getCarsInfo = (selectedCars: ISelectedCars) => {
  const cars = selectedCars.filter(({ quantity }) => quantity > 0);
  return {
    cars,
    quantity: cars.reduce((s, { quantity }) => s + quantity, 0),
    sum: selectedCars.reduce(
      (s, { quantity, price }) => s + quantity * price,
      0
    ),
  };
};

interface Props extends HTMLAttributes<HTMLDivElement> {
  cars: ICar[];
  application: IApplication;
  dialogOpened: boolean;
  setDialogOpened: Function;
  messages?: any;
}

const Application: React.FC<Props> = ({
  cars,
  application,
  dialogOpened,
  setDialogOpened,
  messages,
}) => {
  const [selectedCars, setSelectedCars] = useState<ISelectedCars>(
    cars.map((car, key) => ({ ...car, quantity: key === 0 ? 1 : 0 }))
  );
  const select = (key: number, quantity: number) => {
    let newValue = [...selectedCars];
    newValue[key] = { ...newValue[key], quantity };
    const { quantity: allQuantity } = getCarsInfo(newValue);
    if (allQuantity > 0) setSelectedCars(newValue);
  };
  const { quantity, cars: userCars } = getCarsInfo(selectedCars);
  const { width } = useWindowSize();
  const mobile = width < 1280;

  return (
    <div className={styles.wrap}>
      <div className={styles.left} id="application">
        <div className={styles.title}>Оставить заявку</div>
        <div className={styles.sliderContainer}>
          <Slider
            slidesToShow={1}
            slidesToScroll={1}
            arrows={false}
            lazyLoad="progressive"
            centerMode={true}
            className={styles.selectCar}
          >
            {selectedCars.map((car, key) => (
              <CarCard
                {...car}
                quantity={selectedCars[key].quantity}
                select={select}
                index={key}
                key={key}
                mobile={mobile}
              />
            ))}
          </Slider>
        </div>
        <SelectedCars selectedCars={selectedCars} />
        <Calculator
          {...application}
          cars={userCars}
          dialogOpened={dialogOpened}
          setDialogOpened={setDialogOpened}
          messages={messages}
        />
      </div>
      <div className={styles.right}>
        <CarList cars={cars} selectedCars={selectedCars} select={select} />
      </div>
      <div className={styles.hint2}>
        *Сумма инвестиций кратна стоимости{" "}
        {quantity > 1 ? "выбранных" : "выбранного"} авто
      </div>
    </div>
  );
};

interface SelectedCarsProps extends HTMLAttributes<HTMLDivElement> {
  selectedCars: ISelectedCars;
}

const SelectedCars: React.FC<SelectedCarsProps> = ({ selectedCars }) => {
  const { cars, quantity, sum } = getCarsInfo(selectedCars);
  const count = cars.length;
  return (
    <div className={styles.selectedCars}>
      <div className={styles.carsGrid}>
        {cars.map(({ image, carFullName, quantity }, key) => (
          <div
            className={[
              styles.image,
              count > 4
                ? styles.quarter
                : count > 2
                ? styles.triple
                : count > 1
                ? styles.twin
                : "",
            ].join(" ")}
            key={key}
          >
            <div className={styles.quantity}>x{quantity}</div>
            <Image
              src={image}
              layout="fill"
              objectFit="contain"
              alt={carFullName}
            ></Image>
          </div>
        ))}
      </div>
      <div className={styles.description}>
        <table>
          <tbody>
            {cars.map(({ carFullName, quantity }, key) => (
              <tr key={key}>
                <td
                  className={[styles.carTitle, styles.quantitySmall].join(" ")}
                >
                  {quantity}&nbsp;x
                </td>
                <td className={styles.carTitle}>{carFullName}&nbsp;</td>
                <td className={[styles.carTitle, styles.quantity].join(" ")}>
                  x&nbsp;{quantity}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <div className={styles.carPrice}>
          <Price className={styles.price} price={sum} />
          <div className={styles.hint}>
            Стоимость {quantity > 1 ? "новых автомобилей" : "нового автомобиля"}
          </div>
        </div>
      </div>
    </div>
  );
};

interface CarListProps extends HTMLAttributes<HTMLDivElement> {
  cars: ICar[];
  selectedCars: ISelectedCars;
  select: Function;
}

const CarList: React.FC<CarListProps> = ({ cars, selectedCars, select }) => {
  return (
    <div className={styles.selectCar}>
      {cars.map((car, key) => (
        <CarCard
          {...car}
          quantity={selectedCars[key].quantity}
          select={select}
          index={key}
          key={key}
        />
      ))}
    </div>
  );
};

interface CarCardProps extends HTMLAttributes<HTMLDivElement>, ICar {
  quantity: number;
  index: number;
  select: Function;
  mobile?: boolean;
}

const CarCard: React.FC<CarCardProps> = ({
  carTitle,
  price,
  image,
  quantity,
  select,
  index,
  mobile = false,
}) => {
  return (
    <div
      className={[styles.carCard, quantity ? styles.selected : ""].join(" ")}
      onClick={() => select(index, quantity ? 0 : 1)}
    >
      <div className={styles.description}>
        {quantity > 0 && (
          <div className={styles.checkIcon}>
            <Image src={mobile ? checkInverseIcon : checkIcon} alt="" />
          </div>
        )}
        <div className={styles.carTitle}>{carTitle}</div>
        <Price price={price} className={styles.price} />
        {quantity > 0 && (
          <Quantity change={(delta: number) => select(index, quantity + delta)}>
            {quantity}
          </Quantity>
        )}
      </div>
      <div className={styles.imageContainer}>
        <div className={styles.image}>
          <Image src={image} layout="fill" objectFit="contain" alt={carTitle} />
        </div>
      </div>
    </div>
  );
};

interface QuantityProps extends HTMLAttributes<HTMLDivElement> {
  change: Function;
  children: number;
}

const Quantity: React.FC<QuantityProps> = ({ children, change }) => (
  <div className={styles.quantity}>
    <div
      className={styles.minus}
      onClick={(e) => {
        e.stopPropagation();
        change(-1);
      }}
    >
      -
    </div>
    <div className={styles.number}>{children}</div>
    <div
      className={styles.plus}
      onClick={(e) => {
        e.stopPropagation();
        change(1);
      }}
    >
      +
    </div>
  </div>
);

export default Application;
