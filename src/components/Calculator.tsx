import React, { HTMLAttributes, useState, useEffect } from "react";
import Price from "components/common/Price";
import Image from "next/image";
import Link from "next/link";
import InputMask from "react-input-mask";
import { IApplication, IOptions, ISelectedCars } from "types";
import applicationIcon from "assets/icons/application.svg";
import scheduleIcon from "assets/icons/schedule.svg";
import signIcon from "assets/icons/sign.svg";
import Button from "components/common/Button";
import Select from "components/common/Select";
import RadioGroup from "components/common/RadioGroup";
import Modal from "components/common/Modal";
import Flash from "components/common/Flash";
import { useFormik } from "formik";
import * as Yup from "yup";
import { postToJira } from "api";

import styles from "assets/styles/components/Calculator.module.scss";

const phoneRegexp = /^\+7 \(([0-9]){3}\) ([0-9]){3}-([0-9]){2}-([0-9]){2}$/gm;

interface Props extends HTMLAttributes<HTMLDivElement>, IApplication {
  cars: ISelectedCars;
  dialogOpened: boolean;
  setDialogOpened: Function;
  messages?: any;
}

const Calculator: React.FC<Props> = ({
  schedule,
  towns,
  term,
  cars,
  dialogOpened,
  setDialogOpened,
  messages,
}) => {
  const [options, setOptions] = useState<IOptions>({
    cars,
    term: term[0],
    schedule: schedule[0],
    town: undefined,
  });

  const [flash, setFlash] = useState<any>();

  useEffect(() => {
    setOptions((opt: any) => ({ ...opt, cars }));
  }, [cars]);

  const isValid = (options: any) => {
    return !!options.town;
  };

  const submit = async (values: any) => {
    await postToJira({ ...values, ...options });
    setDialogOpened(false);
  };

  return (
    <>
      <div className={styles.wrap}>
        <div className={styles.title}>
          <Image
            alt=""
            src={applicationIcon}
            width={40}
            height={40}
            layout="fixed"
          />
          <div>Рассчитайте ежемесячный график выплат</div>
        </div>

        <div className={styles.form}>
          <div className={styles.formItem}>
            <div className={styles.formItemInner}>
              <div className={styles.label}>1. Город работы в авто</div>
              <Select
                className={styles.select}
                placeholder="Выберите город"
                values={towns.map((i) => ({ title: i, value: i }))}
                selected={options.town}
                onChange={(e: string) => setOptions({ ...options, town: e })}
              />
            </div>
          </div>
          <div className={styles.formItem}>
            <div className={styles.formItemInner}>
              <div className={styles.label}>2. График погашения</div>
              <Select
                className={styles.select}
                selected={options.schedule}
                values={schedule.map((i) => ({ title: i, value: i }))}
                onChange={(e: string) =>
                  setOptions({ ...options, schedule: e })
                }
              />
            </div>
          </div>
          <div className={styles.formItem}>
            <div className={styles.formItemInner}>
              <div className={styles.label}>3. Срок действия</div>
              <RadioGroup
                className={styles.radio}
                buttons={term}
                active={
                  options?.term
                    ? term.find((i) => i === options?.term)
                    : term[0]
                }
                onChange={(i: string) => setOptions({ ...options, term: i })}
              />
            </div>
          </div>
        </div>

        <div className={styles.bottom}>
          <Link
            href={{
              pathname: "/api/calc",
              query: {
                ...options,
                cars: options.cars.map(
                  ({ carFullName, price, quantity, image }) =>
                    `${carFullName}_${price}_${quantity}_${image}`
                ),
              },
            }}
            passHref
          >
            <a
              target="_blank"
              className={styles.schedule}
              rel="noopener nofollow noreferrer noindex"
            >
              <Image
                alt=""
                src={scheduleIcon}
                width={40}
                height={40}
                layout="fixed"
              />
              <div>График выплат</div>
            </a>
          </Link>
          <Button
            className={styles.button}
            action={() => setDialogOpened(true)}
            disabled={!isValid(options)}
          >
            Отправить заявку
          </Button>
        </div>
      </div>
      {dialogOpened && (
        <Modal close={() => setDialogOpened(false)} className={styles.modal}>
          <div className={styles.title}>Оставить заявку</div>
          {!options.town ? (
            <div className={styles.hint}>
              Наш специалист свяжется с Вами в течение 48 часов и предложит
              варианты сотрудничества
            </div>
          ) : (
            <>
              <Card {...options} />
              <div className={styles.subtitle}>
                Оставьте ваши контактные данные
              </div>
              <div className={styles.hint}>
                Выбранные вами параметры будут переданы менеджеру. Наш
                специалист свяжется с Вами в течение 48 часов и предложит
                варианты сотрудничества
              </div>
            </>
          )}
          <Form setFlash={setFlash} submit={submit} messages={messages} />
          <a
            href="#application"
            className={styles.addon}
            onClick={() => setDialogOpened(false)}
          >
            <Image
              alt="sign"
              src={signIcon}
              width={25}
              height={25}
              layout="fixed"
            />
            <span>
              {!options.town
                ? "Заполнить финансовые параметры"
                : "Уточнить финансовые параметры"}
            </span>
          </a>
        </Modal>
      )}
      {flash && (
        <Flash
          type={flash.type}
          message={flash.message}
          close={() => setFlash(undefined)}
        />
      )}
    </>
  );
};

interface FormProps extends HTMLAttributes<HTMLFormElement> {
  submit?: Function;
  setFlash?: Function;
  messages?: { fail: string; success: string };
}

const Form: React.FC<FormProps> = ({ submit, setFlash, messages }) => {
  const [disabled, setDisabled] = useState(false);
  const formik = useFormik({
    initialValues: {
      name: "",
      msisdn: "",
      email: "",
      comments: "",
    },
    validationSchema: Yup.object({
      name: Yup.string().required("Обязательное поле"),
      msisdn: Yup.string()
        .matches(phoneRegexp, "Неверный номер")
        .required("Обязательное поле"),
      email: Yup.string()
        .email("Неверный адрес электронной почты")
        .required("Обязательное поле"),
    }),
    onSubmit: async (values, { resetForm }) => {
      setDisabled(true);
      try {
        submit && (await submit(values));
        setFlash &&
          setFlash({
            message: messages?.success,
            type: "success",
          });
        // resetForm();
      } catch (ex) {
        console.error(ex);
        setFlash && setFlash({ message: messages?.fail, type: "error" });
        setDisabled(false);
      }
    },
  });
  return (
    <form className={styles.form} autoComplete="off">
      <label htmlFor="name">
        <span>Ваше ФИО*</span>
        <input
          type="text"
          placeholder="Иванов Иван Иванович"
          {...formik.getFieldProps("name")}
          className={
            formik.touched.name && !!formik.errors.name ? styles.errored : ""
          }
        />
      </label>
      <div className={styles.twoColumns}>
        <label htmlFor="msisdn">
          <span>Ваш номер телефона*</span>
          <InputMask
            mask="+7 (999) 999-99-99"
            placeholder="+7 (___) ___-__-__"
            {...formik.getFieldProps("msisdn")}
            className={
              formik.touched.msisdn && !!formik.errors.msisdn
                ? styles.errored
                : ""
            }
          />
        </label>
        <div className={styles.spacer} />
        <label htmlFor="email">
          <span>Ваш email*</span>
          <input
            type="text"
            placeholder="ivanov@mail.ru"
            {...formik.getFieldProps("email")}
            className={
              formik.touched.email && !!formik.errors.email
                ? styles.errored
                : ""
            }
          />
        </label>
      </div>
      <label htmlFor="comments">
        <span>Комментарий</span>
        <textarea name="comments" placeholder="Оставить пожелание..." />
      </label>
      <div className={styles.footer}>
        <span>
          Нажимая на кнопку, вы даете согласие на обработку ваших персональных
          данных
        </span>
        <div className={styles.spacer} />
        <Button action={() => formik.submitForm()} disabled={disabled}>
          Оставить заявку
        </Button>
      </div>
    </form>
  );
};

interface CardProps extends HTMLAttributes<HTMLFormElement> {
  cars: ISelectedCars;
  term: string;
  schedule: string;
}

const Card: React.FC<CardProps> = ({ cars, term }) => {
  const sum = cars.reduce((s, { quantity, price }) => s + quantity * price, 0);
  const period = parseInt(term);
  const car = cars[0];
  return (
    <div className={styles.card}>
      <div className={styles.container}>
        <div className={styles.cars}>
          <table className={styles.carList}>
            <tbody>
              {cars.map(({ carTitle, carFullName, quantity }) => (
                <tr className={styles.carItem} key={carTitle}>
                  <td>{carFullName}</td>
                  <td>x {quantity}</td>
                </tr>
              ))}
            </tbody>
          </table>
          <div className={styles.sum}>
            <Price price={sum} />
          </div>
        </div>

        <div className={[styles.auto, styles.auto_mobile].join(" ")}>
          <div className={styles.term}>{term}</div>
          <Image
            src={car.image}
            alt={car.image}
            layout="fill"
            objectFit="contain"
          />
        </div>

        <div className={styles.info}>
          <div>
            <div className={styles.header}>Ежемесячные выплаты</div>
            <Price price={sum / period} className={styles.value} />
          </div>
          <div className={styles.spacer} />
          <div>
            <div className={styles.header}>Процентная ставка</div>
            <div className={styles.value}>{Math.ceil(13 - period / 10)}%</div>
          </div>
        </div>
      </div>
      <div className={[styles.auto, styles.auto_desktop].join(" ")}>
        <div className={styles.term}>{term}</div>
        <Image
          src={car.image}
          alt={car.image}
          layout="fill"
          objectFit="contain"
        />
      </div>
    </div>
  );
};

export default Calculator;
