import React, { HTMLAttributes } from "react";
import Button from "components/common/Button";

import styles from "assets/styles/components/common/RadioGroup.module.scss";

interface Props {
  className?: string;
  buttons: string[];
  active?: string;
  onChange?: Function;
}

const RadioGroup: React.FC<Props> = ({
  className,
  buttons,
  active,
  onChange,
}) => {
  return (
    <div className={[styles.wrap, className].join(" ")}>
      {buttons.map((i) => (
        <Button
          type={i === active ? "light" : "disabled"}
          className={styles.button}
          key={i}
          action={() => onChange && onChange(i)}
        >
          {i}
        </Button>
      ))}
    </div>
  );
};

export default RadioGroup;
