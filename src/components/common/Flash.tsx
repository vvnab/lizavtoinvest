import React, { HTMLAttributes, useEffect } from "react";

import styles from "assets/styles/components/common/Flash.module.scss";

interface Props extends HTMLAttributes<HTMLDivElement> {
  message?: string;
  close?: Function;
  type?: "success" | "error";
}

const Flash: React.FC<Props> = ({ className, message, close, type }) => {
  useEffect(() => {
    setTimeout(() => close && close(), 5000);
  }, []);
  return (
    <div className={styles.wrap}>
      <div
        className={[
          styles.flash,
          type === "success"
            ? styles.success
            : type === "error"
            ? styles.error
            : "",
          className,
        ].join(" ")}
      >
        {" "}
        {message}
      </div>
    </div>
  );
};

export default Flash;
