import React, { HTMLAttributes, useState } from "react";
import Image from "next/image";
import closeIcon from "assets/icons/close.svg";
import chevronDownIcon from "assets/icons/chevronDown.svg";
import { IFaq, IFaqs } from "types";

import styles from "assets/styles/components/Faqs.module.scss";

interface Props extends HTMLAttributes<HTMLDivElement> {
  faqs: IFaqs;
}

const Faqs: React.FC<Props> = ({ faqs }) => {
  return (
    <div className={styles.wrap}>
      <div className={styles.title}>Остались вопросы?</div>
      {faqs.map((faq, key) => (
        <Item {...faq} key={key}/>
      ))}
    </div>
  );
};

type IItemProps = IFaq & HTMLAttributes<HTMLDivElement>;

const Item: React.FC<IItemProps> = ({ question, answer }) => {
  const [opened, setOpened] = useState(false);
  return (
    <div
      className={[styles.itemWrap, opened ? styles.opened : ""].join(" ")}
      onClick={() => setOpened(!opened)}
    >
      <div className={styles.item}>
        <div className={styles.question}>{question}</div>
        <div className={styles.answer}>{answer}</div>
      </div>
      <div className={styles.icon}>
        <Image  alt="" src={opened ? closeIcon : chevronDownIcon}/>
      </div>
    </div>
  );
};

export default Faqs;
