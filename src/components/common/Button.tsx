import React, { HTMLAttributes } from "react";

import styles from "assets/styles/components/common/Button.module.scss";

interface Props extends HTMLAttributes<HTMLDivElement> {
  type?: "basic" | "light" | "dark" | "invert" | "disabled" | "simple";
  action?: Function;
  href?: string;
  disabled?: boolean;
}

const Button: React.FC<Props> = ({
  className,
  children,
  href,
  action,
  type = "basic",
  disabled = false,
}) =>
  href ? (
    <a href={href} className={[styles.wrap, styles[type], className].join(" ")}>
      {children}
    </a>
  ) : (
    <div
      className={
        type !== "simple"
          ? [
              styles.wrap,
              styles[type],
              disabled ? styles.disabled : "",
              className,
            ].join(" ")
          : className
      }
      onClick={(e: any) => (!disabled && action ? action(e) : {})}
    >
      {children}
    </div>
  );

export default Button;
