export interface IDownload {
  title: string;
  src: string;
}

export interface IHowItWorks {
  car: string;
  sign: string;
  rent: string;
  prolongation: string;
  percent: string;
  contract: string;
}

export interface ICar {
  carTitle: string;
  carFullName: string;
  price: number;
  image: string;
}

export interface IReason {
  title: string;
  description?: string;
}

export interface IReasons {
  risks: IReason[];
  pluses: IReason[];
  competitorsRisks: IReason[];
  competitorsPluses: IReason[];
}

export interface IContacts {
  phone: string;
  address: string;
}

export interface IFaq {
  question: string;
  answer: string;
}

export type IFaqs = IFaq[];

export interface IBegin {
  title: string;
  description: string;
}

export interface IHero {
  title: string;
  description: string;
  button: string;
}

export interface IMeta {
  title: string;
  description: string;
}

export interface IApplication {
  towns: string[];
  schedule: string[];
  term: string[];
}

export interface IOptions {
  cars: ICarQuantity[];
  town: string | undefined;
  term: string;
  schedule: string;
}

export interface ICarQuantity extends ICar {
  quantity: number;
}

export type ISelectedCars = Array<ICarQuantity>;

export interface ISelect {
  title: string;
  value: any;
}
