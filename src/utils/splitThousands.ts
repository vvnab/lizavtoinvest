export default function breaksHundreds(value: string | number): string {
  return `${value}`.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1\u00A0')
}