import React, { HTMLAttributes } from "react";
import Button from "components/common/Button";
import { IHero } from "types";

import styles from "assets/styles/components/Hero.module.scss";

interface Props extends IHero {
  openDialog?: Function;
}

const Hero: React.FC<Props> = ({ title, description, button, openDialog }) => (
  <div className={styles.wrap}>
    <div className={styles.inner}>
      <div className={styles.left}>
        <div className={styles.title}>{title}</div>
        <div className={styles.description}>{description}</div>
        <Button className={styles.button} action={openDialog}>
          {button}
        </Button>
      </div>
      <div className={styles.imgContainer} />
      <Button className={styles.button_2} type="invert" action={openDialog}>
        {button}
      </Button>
      <div className={styles.right}></div>
    </div>
  </div>
);

export default Hero;
