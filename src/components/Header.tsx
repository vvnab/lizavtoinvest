import React, { HTMLAttributes, useState } from "react";
import useWindowSize from "hooks/useWindowSize";
import { IContacts, IDownload } from "types";
import Image from "next/image";
import kebabIcon from "assets/icons/kebab.svg";
import timesIcon from "assets/icons/timesGray.svg";
import Logo from "components/common/Logo";
import Download from "components/common/Download";
import Contacts from "components/common/Contacts";
import Modal from "components/common/Modal";

import styles from "assets/styles/components/Header.module.scss";

interface Props extends HTMLAttributes<HTMLDivElement> {
  downloads: IDownload[];
  contacts: IContacts;
}

const Header: React.FC<Props> = ({ downloads, contacts }) => {
  const { width } = useWindowSize();
  const isMobile = width < 1280;
  const isDesktop = !isMobile;
  const [menuOpen, setMenuOpen] = useState(false);
  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
  };
  return (
    <div className={styles.wrap}>
      <div className={styles.left}>
        {menuOpen && (
          <Modal
            close={() => setMenuOpen(false)}
            noTimes
            closeOnClickOutside
            className={styles.mobileMenu}
          >
            <div className={styles.logo}>
              <Logo />
              <div
                className={styles.times}
                onClick={() => isMobile && toggleMenu()}
              >
                <Image src={timesIcon} alt="times" layout="fill" />
              </div>
            </div>
            <div className={styles.downsList}>
              {downloads.map(({ title, src }, key) => (
                <Download
                  title={title}
                  src={src}
                  key={key}
                  className={styles.downsItem}
                />
              ))}
            </div>
            <hr />
            <Contacts {...contacts} className={styles.contacts} invert />
          </Modal>
        )}
        <div className={styles.kebab} onClick={() => isMobile && toggleMenu()}>
          <Image src={kebabIcon} alt="kebab" layout="fill" />
        </div>
        <Logo />
        {isDesktop &&
          downloads.map(({ title, src }, key) => (
            <Download title={title} src={src} key={key} />
          ))}
      </div>
      <div className={styles.right}>
        <Contacts
          className={styles.contacts}
          phone={contacts.phone}
          address={contacts.address}
        />
      </div>
    </div>
  );
};

export default Header;
