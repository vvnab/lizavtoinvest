import React, { HTMLAttributes } from "react";
import splitThousends from "utils/splitThousands";

import styles from "assets/styles/components/common/Price.module.scss";

interface IProps extends HTMLAttributes<HTMLDivElement> {
  price: number | string;
}

const Price: React.FC<IProps> = ({ className, price }) => (
  <div className={[styles.wrap, className].join(" ")}>
    {splitThousends(parseInt(price.toString()))}&nbsp;&#8381;
  </div>
);

export default Price;
