import React, { HTMLAttributes } from "react";
import Image from "next/image";
import phoneIcon from "../../assets/icons/phone.svg";
import locationIcon from "../../assets/icons/location.svg";
import { IContacts } from "types";

import styles from "assets/styles/components/common/Contacts.module.scss";

type Props = IContacts & HTMLAttributes<HTMLDivElement>;
interface IProps extends Props {
  invert?: boolean;
}

const Contacts: React.FC<IProps> = ({ className, phone, address, invert }) => (
  <div className={[styles.wrap, className].join(" ")}>
    <div className={styles.phone}>
      <div
        className={[styles.phoneIcon, invert ? styles.invert : ""].join(" ")}
      >
        <Image alt="" src={phoneIcon} />
      </div>
      {phone}
    </div>
    <div className={styles.address}>
      <div
        className={[styles.addressIcon, invert ? styles.invert : ""].join(" ")}
      >
        <Image alt="" src={locationIcon} />
      </div>
      {address}
    </div>
  </div>
);

export default Contacts;
