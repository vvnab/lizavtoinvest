import React from "react";
import {
  Page,
  Text,
  View,
  Document,
  Image,
  StyleSheet,
  Font,
} from "@react-pdf/renderer";
import splitThousands from "utils/splitThousands";

Font.register({
  family: "Gilroy",
  src: process.env.HOST + "/Gilroy-Regular.ttf",
});

const getCar = (i: any): any => {
  let [name, price, quantity, imgUrl] = i.split("_");
  imgUrl = process.env.HOST + imgUrl.replace("cars", "thumbs");
  const sum = parseInt(price) * parseInt(quantity);
  return { name, price, quantity, imgUrl, sum };
};

const styles = StyleSheet.create({
  page: {
    flexDirection: "column",
    backgroundColor: "white",
    fontFamily: "Gilroy",
    fontSize: "14pt",
  },
  section: {
    margin: 20,
    padding: 20,
    flexGrow: 1,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  car: {
    alignItems: "center",
    justifyContent: "flex-end",
    fontSize: "12pt",
    height: "130px",
  },
  img: {
    width: "150px",
    marginBottom: 10,
  },
  table: {
    flexDirection: "column",
    justifyContent: "flex-start",
    flexGrow: 0,
  },
  row: {
    flexDirection: "row",
    borderBottom: "1px solid black",
  },
  rowFirst: {
    borderTop: "1px solid black",
  },
  cell: {
    flexDirection: "row",
    padding: "5px 10px",
    borderRight: "1px solid black",
    borderLeft: "none",
    width: "100%",
  },
  cellFirst: {
    borderLeft: "1px solid black",
  },
  tableFirst: {
    width: "400px",
  },
  tableSecond: {
    width: "300px",
  },
});

interface IProps {
  cars: string[];
  term: string | string[];
  town: string | string[];
  schedule: string | string[];
}

const Calc: React.FC<IProps> = ({ town, term, schedule, cars }) => (
  <Document language="ru">
    <Page size="A4" orientation="landscape" wrap={false} style={styles.page}>
      <View style={styles.section}>
        {cars.map((i, k) => {
          const { imgUrl, name, sum, quantity } = getCar(i);
          return (
            <View key={k} style={styles.car}>
              {/*eslint-disable */}
              <Image src={imgUrl} style={styles.img} />
              {/* eslint-enable */}
              <Text>{name}</Text>
              <Text>x{quantity}</Text>
              <Text>{splitThousands(sum)}</Text>
            </View>
          );
        })}
      </View>

      {/* Раздел таблиц */}
      <View style={[styles.section]}>
        {/* таблица 1 */}
        <View style={[styles.table, styles.tableFirst]}>
          <View style={[styles.row, styles.rowFirst]}>
            <View style={[styles.cell, styles.cellFirst]}>
              <Text>Город работы авто:</Text>
            </View>
            <View style={styles.cell}>
              <Text>{town}</Text>
            </View>
          </View>
          <View style={styles.row}>
            <View style={[styles.cell, styles.cellFirst]}>
              <Text>График погашения:</Text>
            </View>
            <View style={styles.cell}>
              <Text>{schedule}</Text>
            </View>
          </View>
        </View>
        {/* таблица 2 */}
        <View style={[styles.table, styles.tableSecond]}>
          <View style={[styles.row, styles.rowFirst]}>
            <View style={[styles.cell, styles.cellFirst, { width: "200%" }]}>
              <Text>Срок действия договора:</Text>
            </View>
            <View style={styles.cell}>
              <Text>{term}</Text>
            </View>
          </View>
          <View style={styles.row}>
            <View style={[styles.cell, styles.cellFirst, { width: "200%" }]}>
              <Text>Процент годовых:</Text>
            </View>
            <View style={styles.cell}>
              <Text>10%</Text>
            </View>
          </View>
        </View>
      </View>
    </Page>
  </Document>
);

export default Calc;
