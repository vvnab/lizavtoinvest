import * as fs from "fs/promises";
import path from "path";
import yaml from "js-yaml";


export function parseContent(text: string) {
  const content = yaml.load(text)
  return content;
}

export async function loadContentFile(file: string) {
  const filePath = path.join(process.cwd(), "src/content", `${file}`);
  const fileContent = await fs.readFile(filePath, "utf8");
  return parseContent(fileContent);
}

export async function loadManyContentFiles(files: string[]) {
  return await Promise.all(files.map((file) => loadContentFile(file)));
}
