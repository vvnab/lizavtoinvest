import React, { HTMLAttributes } from "react";
import Image from "next/image";
import carIcon from "assets/icons/car.svg";
import signIcon from "assets/icons/sign.svg";
import rentIcon from "assets/icons/rent.svg";
import prolongationIcon from "assets/icons/prolongation.svg";
import aggregators from "assets/images/aggregators.svg";
import Button from "components/common/Button";
import Download from "components/common/Download";
import useWindowSize from "hooks/useWindowSize";
import { IHowItWorks, IDownload } from "types";

import styles from "assets/styles/components/HowItWorks.module.scss";

type Props = IHowItWorks & HTMLAttributes<HTMLDivElement>;
interface IProps extends Props {
  downloads: IDownload[];
  openDialog?: Function;
}

const HowItWorks: React.FC<IProps> = ({
  car,
  sign,
  rent,
  prolongation,
  percent,
  contract,
  downloads,
  openDialog,
}) => {
  const { width } = useWindowSize();
  const isDesktop = width >= 1280;
  return (
    <div className={styles.wrap}>
      <div className={styles.left}>
        <div className={styles.conditions}>
          <div>{percent}</div>
          <div>{contract}</div>
        </div>

        <div className={styles.title}>Как это работает?</div>
        <div className={styles.list}>
          <div className={styles.line} />
          <div className={styles.item}>
            <div className={styles.image}>
              <Image
                alt=""
                src={carIcon}
                width={40}
                height={40}
                layout="fixed"
              />
            </div>
            <div className={styles.description}>{car}</div>
          </div>

          <div className={styles.item}>
            <div className={styles.image}>
              <Image
                alt=""
                src={signIcon}
                width={40}
                height={40}
                layout="fixed"
              />
            </div>
            <div className={styles.description}>{sign}</div>
          </div>

          <div className={styles.item}>
            <div className={styles.image}>
              <Image
                alt=""
                src={rentIcon}
                width={40}
                height={40}
                layout="fixed"
              />
            </div>
            <div className={styles.description}>{rent}</div>
          </div>

          <div className={styles.item}>
            <div className={styles.image}>
              <Image
                alt=""
                src={prolongationIcon}
                width={40}
                height={40}
                layout="fixed"
              />
            </div>
            <div className={styles.description}>{prolongation}</div>
          </div>
        </div>
      </div>
      <div className={styles.right}>
        <div className={styles.aggregators}>
          <Image alt="" src={aggregators} />
        </div>

        <div className={styles.conditions}>
          <div>{percent}</div>
          <div>{contract}</div>
        </div>

        <div className={styles.guard}>
          Защита Ваших инвестиций!
          <Button
            type={isDesktop ? "invert" : "basic"}
            className={styles.button}
            action={openDialog}
          >
            Хочу попробовать
          </Button>
        </div>
        <div className={styles.downloads}>
          {downloads.map(({ title, src }, key) => (
            <Download
              invert={isDesktop}
              title={title}
              src={src}
              className={styles.item}
              key={key}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default HowItWorks;
