import React, { HTMLAttributes } from "react";
import Image from "next/image";
import logog from "../../assets/icons/logo.svg";

import styles from "assets/styles/components/common/Logo.module.scss";

type Props = HTMLAttributes<HTMLDivElement>;

const Logo: React.FC<Props> = ({ className }) => (
  <div className={[styles.wrap, className].join(" ")}>
    <Image alt="" src={logog} />
  </div>
);

export default Logo;
