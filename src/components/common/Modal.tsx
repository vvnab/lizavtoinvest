import React, { HTMLAttributes, useRef, useEffect } from "react";
import Image from "next/image";
import timesIcon from "assets/icons/times.svg";

import styles from "assets/styles/components/common/Modal.module.scss";

interface IProps extends HTMLAttributes<HTMLDivElement> {
  close: Function;
  noTimes?: boolean;
  closeOnClickOutside?: boolean;
}

const Modal: React.FC<IProps> = ({
  className,
  children,
  close,
  noTimes = false,
  closeOnClickOutside = false,
}) => {
  const el = useRef(null);
  const handleClickOutside = (e: MouseEvent) => {
    // @ts-ignore
    if (el && el.current && el.current.contains(e.target)) {
      return;
    } else {
      closeOnClickOutside && close();
    }
  };

  useEffect(() => {
    const isDesktop = window.innerWidth > 1280;
    document.addEventListener("mousedown", handleClickOutside, false);
    document.body.style.overflowY = "hidden";
    if (isDesktop) document.body.style.marginRight = "16px";
    return () => {
      document.body.style.overflowY = "auto";
      if (isDesktop) document.body.style.marginRight = "0";
      document.removeEventListener("mousedown", handleClickOutside, false);
    };
  }, []);

  return (
    <div className={styles.overlay}>
      {noTimes ? (
        <div className={[styles.wrap, className].join(" ")} ref={el}>
          {children}
        </div>
      ) : (
        <div className={styles.container}>
          <div>
            <div className={styles.close} onClick={() => close()}>
              Закрыть
              <div className={styles.times}>
                <Image src={timesIcon} alt="" />
              </div>
            </div>
            )
            <div className={[styles.wrap, className].join(" ")} ref={el}>
              {children}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Modal;
